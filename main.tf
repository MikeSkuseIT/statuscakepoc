provider "statuscake" {
  username = "${var.username}"
  apikey   = "${var.apikey}"
}

resource "statuscake_test" "Ops Kibana" {
  website_name  = "OpsKibana"
  website_url   = "www.google.co.uk"
  test_type     = "HTTP"
  check_rate    = 300
  contact_id    = 129380
  timeout       = 2
  trigger_rate  = 1
  confirmations = 2
}
