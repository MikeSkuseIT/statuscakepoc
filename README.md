# StatusCake Terraform POC
This is a POC / test for using Terraform to control StatusCake monitoring. It is a starting place to show how terraform files should be structured in order to add / adjust monitoring under a StatusCake account.

## Files
Required files are:
* main.tf - Stores the main components of the tests including URL - currently this is titled Kibana but directing to the nationwide.co.uk URL until the correct URL is identified.
* variables.tf - This defines the variables to be used by main.tf
* variables.tfvars - This file stores the actual contents of the variables, it is called when using terraform and can vary between environments - i.e. using different variable contents for each environment.

## Usage
Standard terraform useage:
```
terraform plan -var-file=/users/user/variables.tfvars
```

## Next steps
* Create nginx ingress route to allow StatusCake to communicate with services inside the cluster.